<?php

/**
 * @file
 * Admin settings
 */

/**
 * Administration form. 
 */
function issue_tree_admin_form($form_state) {
  $form = array();
  $form['lookup'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lookup for sub-issues'),
  );
  $form['lookup']['issue_tree_lookup_after'] = array(
    '#type' => 'textfield',
    '#title' => t('After this string'),
    '#default_value' => variable_get('issue_tree_lookup_after', '<h3>Sub-issues</h3>'),
    '#required' => TRUE
  );
  $form['lookup']['issue_tree_lookup_until'] = array(
    '#type' => 'textfield',
    '#title' => t('Until this string'),
    '#description' => t('If the node body does not contain this string after the above specified string, the lookup will be extended until the end of the node body.'),
    '#default_value' => variable_get('issue_tree_lookup_until', '<h3>'),
    '#required' => TRUE
  );
  
  $query = "SELECT v.vid, v.name FROM {vocabulary} v 
    LEFT JOIN {vocabulary_node_types} n ON v.vid = n.vid WHERE n.type = '%s' ORDER BY v.weight, v.name";
  $result = db_query(db_rewrite_sql($query, 'v', 'vid'), 'project_issue');
  while ($voc = db_fetch_object($result)) {
    $vocabularies[$voc->vid] = check_plain($voc->name);
  }
  if ($vocabularies) {
    $form['issue_tree_vocabularies'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Vocabularies'),
      '#description' => t('Show taxonomy terms in the issue summary outline from the above specified vocabularies.'),
      '#options' => $vocabularies,
      '#default_value' => variable_get('issue_tree_vocabularies', array()),
    );  
  }
  else {
    $form['issue_tree_vocabularies_info'] = array(
      '#type' => 'item',
      '#value' => t('If you <a href="@taxonomy-admin">assign taxonomy vocabularies</a> to the Issue content type to categorize Issue nodes, you can display taxonomy terms in the issue summary outline.',
        array('@taxonomy-admin' => url('admin/content/taxonomy'))),
    );
  }
  return system_settings_form($form);
}
