Issue Tree

Authors:
Balint Kleri (balint kleri hu)
Peter Arato (it.arato gmail com)
Kornel Lugosi (coornail gmail com)

Install:
- Download the module
- Unpack it under sites/[yoursite]/modules/
- Go to http://thejit.org/
- Download the latest tarball
- Unpack it under sites/all/libraries/jit
- Enable the module
...
- Profit!
