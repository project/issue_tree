/**
 * Customization of the JIT JavaScript plugin.
 */

Drupal.IssueTree = Drupal.IssueTree || {};
Drupal.IssueTree.lastTip = null;
Drupal.IssueTree.tipTimeOut = null;

$(function(){
  Drupal.IssueTree.init();
});

Drupal.IssueTree.init = function() {
    //init Spacetree
    //Create a new ST instance
    var st = new $jit.ST({
        //id of viz container element
        injectInto: 'issue_tree_wrapper',
        //set duration for the animation
        duration: 300,
        //set animation transition type
        transition: $jit.Trans.Quart.easeInOut,
        //set distance between node and its children
        levelDistance: 50,
        //enable panning
        Navigation: {
          enable:true,
          panning:true
        },
        //set node and edge styles
        //set overridable=true for styling individual
        //nodes or edges
        Node: {
            height: 24,
            width: 200,
            type: 'rectangle',
            color: '#DDD',
            overridable: true
        },

        Edge: {
            type: 'bezier'
        },

        //This method is called on DOM label creation.
        //Use this method to add event handlers and styles to
        //your node.
        onCreateLabel: function(label, node){
            label.id = node.id;
            label.innerHTML = node.name + ' <a href="' + Drupal.settings.basePath + 'node/' + node.data.nid + '">[view]</a>';
            label.onclick = function(){st.onClick(node.id);};
            //set label styles
            var style = label.style;
            style.width       = '200px';
            style.height      = '28px';
            style.cursor      = 'pointer';
            style.color       = '#333';
            style.fontSize    = '0.8em';
            style.textAlign   = 'left';
            style.paddingTop  = '0px';
            style.paddingLeft = '30px';
            style['font-weight'] = (node.selected) ? 'bold' : 'normal';
        },

        onBeforePlotNode: function(node){
          // Coloring is according to the Drupal.org issue colors.
          // @todo make it configurable
          switch(node.data.state) {
            case 'active':
              // It should be #f9f9f9, but it's too bright in this context
              node.data.$color = '#DDD';
              break;
            case 'closed (duplicate)':
            case 'closed (works as designed)':
            case 'postponed (maintainer needs more info)':
              node.data.$color = '#EFF1FE';
              break;
            case 'closed (fixed)':
              node.data.$color = '#FDDDDD';
              break;
            case 'needs review':
              node.data.$color = '#FFD';
              break;
            case 'needs work':
              node.data.$color = '#FFECE8';
              break;
            case 'reviewed & tested by the community':
              node.data.$color = '#F1FFE8';
              break;
            default:
              node.data.$color = "#DDD"
          }
        },

        onBeforePlotLine: function(adj){
            if (adj.nodeFrom.selected && adj.nodeTo.selected) {
                adj.data.$color = "#eed";
                adj.data.$lineWidth = 3;
            }
            else {
                delete adj.data.$color;
                delete adj.data.$lineWidth;
            }
        },

        Tips: {
            enable: true,
            type: 'native',
            onShow: function(tip, node) {
                Drupal.IssueTree.lastTip = tip;
                Drupal.IssueTree.lastTip.innerHTML = '';
                clearTimeout(Drupal.IssueTree.tipTimeOut);
                Drupal.IssueTree.tipTimeOut = setTimeout(function(){
                  tip.innerHTML = node.data.issue_summary;
                }, 1000);
            },
            onHide: function() {
              Drupal.IssueTree.lastTip.innerHTML = '';
            }
        }
    });
    //load json data
    st.loadJSON(Drupal.settings.issue_tree);
    //compute node positions and layout
    st.compute();
    //optional: make a translation of the tree
    st.geom.translate(new $jit.Complex(-200, 0), "current");
    //emulate a click on the root node.
    st.onClick(st.root);
}
